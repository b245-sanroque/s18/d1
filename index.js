console.log("Konnichiwa!");

function printInput(){
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi! "+nickname);
}

// printInput();

	// Parameter passing

	function printName(name, age, address){
		console.log("My name is "+name);
		console.log("My age is "+age);
	}

	// Argument
	printName("Aya", "20+");
	printName("Ayka");
	printName();

// [SECTION] Parameters and Arguments

/*
	Parameter
		- "name" is called parameter
		- acts as a named variable/container that only exists inside a function.
		- it is used to store information that is provided to a function when it is called/invoked.

	Arguments
		- "Aya" is the value/data passed directly into the function.
		- Values passed when invoking a function calle arguments.
		- Those argument are then stored as the parameters within the function.
*/

	function checkDivisibilityBy8(num){
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(10);
	checkDivisibilityBy8(32);

	// A variable as the argument
	let num1 = 64;
	checkDivisibilityBy8(num1);

// [SECTION] Function as Arguments
/*
	Function Parameters can also accept other functions as arguments.

	Some complex functions use other functions as arguments to perform more complicated results.

	This be further seen in array methods.
*/
	function argumentFunc(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunc(argFunc){
		// console.log(argFunc);
		argFunc();
	}
	// Adding and removing the parenthesis "()" impacts the output of javascript.
		// function is used with parenthesis it denotes invoke/call.
		// function is used without a parenthesis is normally associated with using a function as a argument to another function.
	invokeFunc(argumentFunc);

// [SECTION] Multiple Parameters
/*
	Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.
*/
	function createFullName(firstName, middleName, lastName){
		console.log(firstName+" "+middleName+" "+lastName)
	}
	createFullName("Juan","Enye","Dela Cruz");
	createFullName("Juan","","Dela Cruz");
	createFullName("Juan","Enye","Dela Cruz","Jr");

	// Using variables as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);

	// NOTE: The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, the second argument will be stored in the second parameter and so on...

// [SECTION] Return Statement
/*
	Return Statements
		- allow us to output a value from a function to be passed to the line/code block of the code that invoked/called the function.

		- we can further use/manipulate in our program instead of only printing/displaying it on the console.
*/

	function returnFullName(firstName,middleName,lastName){
			// expected return, Jeffrey Smith Doe
		return firstName +" "+middleName+" "+lastName;
			// return indicates the end of function execution.
			// it will ignore any codes after return statement.
		console.log(firstName+" "+middleName+" "+lastName);
	}

	let comepleteName = returnFullName("Jeffrey","Smith","Doe");
	console.log(comepleteName);

	console.log("My complete name is "+comepleteName);
	console.log("I am "+comepleteName+". I live in Quezon City");

	
	function checkDivisibilityBy5(num){
		let rem = remainder(num);
		console.log("The remainder of " + num + " divided by 5 is: "+rem);
		let isDivisibleBy5 = remainder === 0;
		console.log("Is " + num + " divisible by 5?");
		console.log(isDivisibleBy5);
	}
	function remainder(num){
		return num % 5;
	}

	checkDivisibilityBy5(21);

	// You can also create a variable inside a function to contain the result and return the variable.

	function returnAddress(city, country){
		let fullAddress = city+", "+country;
		return fullAddress;
	}

	let myAddress = returnAddress("Cebu City", "Cebu")
	console.log(myAddress);
	console.log(returnAddress("Cebu City", "Cebu"));

	// when a function only has a console.log() to display the result it will return undefined instead.

	function printPlayerInfo(username, level, job){
		console.log("Username: "+username);
		console.log("Level: "+level);
		console.log("Job: "+job);
	}

	let user = printPlayerInfo("knight_white", 95, "Paladin");
	// You cannot save any value from printPlayerInfo() because it does not return anything.
	console.log(user);